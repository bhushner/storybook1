import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/angular/types-6-0';

// import { ImageComponent } from '../app/image/image.component';
import {InputBoxComponent} from "../app/input-box/input-box.component";
export default {
  title: 'Example/Input Box Component',
  component: InputBoxComponent,
} as Meta;

const Template: Story<InputBoxComponent> = (args: InputBoxComponent) => ({
  component: InputBoxComponent,
  props: args,
});

export const TextField = Template.bind({});
TextField.args = {
  inpType:"text",
  placeHolder:"Enter the text"
};

export const EmailField = Template.bind({});
EmailField.args = {
    inpType:"email",
    placeHolder:"Enter the email"
};
export const WithOutPlaceHolder = Template.bind({});
WithOutPlaceHolder.args = {
    placeHolder:""
};

export const Width100 = Template.bind({});
Width100.args = {
    BoxWidth:'100%',
    placeHolder:"Enter the text"
};
export const Width50 = Template.bind({});
Width50.args = {
    BoxWidth:'50%',
    placeHolder:"Enter the text"
};
// export const WithZeroOpacity = Template.bind({});
// WithZeroOpacity.args = {
//     inpType:""
// };

// export const WithHalfOpacity = Template.bind({});
// WithHalfOpacity.args = {
//     imgOpacity: 0.5
// };