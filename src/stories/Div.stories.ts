import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/angular/types-6-0';

// import { ImageComponent } from '../app/image/image.component';
// import { isDevMode } from '@angular/core';
import { Div1Component } from 'src/app/div1/div1.component';
export default {
  title: 'Example/DIV Component',
  component: Div1Component,
} as Meta;

const Template: Story<Div1Component> = (args: Div1Component) => ({
  component: Div1Component,
  props: args,
});

export const cngBgRed = Template.bind({});
cngBgRed.args = {
    color1:"red"
};

export const cngBgGreen = Template.bind({});
cngBgGreen.args = {
    color1:"green"
};

export const cngBgYellow = Template.bind({});
cngBgYellow.args = {
    color1:"yellow"
};