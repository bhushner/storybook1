// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-image',
//   templateUrl: './image.component.html',
//   styleUrls: ['./image.component.css']
// })
// export class ImageComponent implements OnInit {

//   constructor() { }

//   ngOnInit(): void {
//   }

// }
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css'],
})
export class ImageComponent implements OnInit {
  @Input()
  imgSrc: string = "../../assets/natureWallpaper.jpg"; //Make sure you add your image URL

  @Input()
  altTxt?: string = 'Nature Wallpaper';

  @Input()
  figCaptionTxt?: string =
    "A mountain is an elevated portion of the Earth's crust, generally with steep sides that show significant exposed bedrock. A mountain differs from a plateau in having a limited summit area, and is larger than a hill, typically rising at least 300 metres (1000 feet) above the surrounding land.";

  @Input()
  imgOpacity?: number = 1;

  constructor() {}

  ngOnInit(): void {}
}
