import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-input-box',
  templateUrl: './input-box.component.html',
  styleUrls: ['./input-box.component.css']
})
export class InputBoxComponent implements OnInit {
  @Input()
  inpType: string = "text";

  @Input()
  placeHolder?: string = '';

  @Input()
  BoxWidth?: string = '';


  // @Input()
  // figCaptionTxt?: string =
  //   "A mountain is an elevated portion of the Earth's crust, generally with steep sides that show significant exposed bedrock. A mountain differs from a plateau in having a limited summit area, and is larger than a hill, typically rising at least 300 metres (1000 feet) above the surrounding land.";

  // @Input()
  // imgOpacity?: number = 1;

  constructor() { }

  ngOnInit(): void {
  }

}

// import {Component} from '@angular/core';
// import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
// import {ErrorStateMatcher} from '@angular/material/core';

// /** Error when invalid control is dirty, touched, or submitted. */
// export class MyErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//     const isSubmitted = form && form.submitted;
//     return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
//   }
// }

// /** @title Input with a custom ErrorStateMatcher */
// @Component({
//   selector: 'app-input',
//   templateUrl: './input-box.component.html',
//   styleUrls: ['./input-box.component.css'],
// })
// export class InputBoxComponent {
//   emailFormControl = new FormControl('', [Validators.required, Validators.email]);

//   matcher = new MyErrorStateMatcher();
// }
