import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-div1',
  templateUrl: './div1.component.html',
  styleUrls: ['./div1.component.css']
})
export class Div1Component implements OnInit {
@Input() color1: string="black";
  constructor() { }

  ngOnInit(): void {
  }

}
