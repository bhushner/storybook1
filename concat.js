/**

* create a JS anc CSS file for hosting web components

* @author: neel shah

*/

const fs = require('fs-extra');

const concat = require('concat');

(async function build() {

const files = [


'./dist/storybook1/main.4eab3f513c4ff7ca.js',
'./dist/storybook1/polyfills.807517c62d459d68.js',
'./dist/storybook1/runtime.6e8527d9b07061ae.js',
// './dist/iata-web-components/runtime.js',
// './dist/iata-web-components/polyfills.js',
// './dist/iata-web-components/main.js',
// './dist/iata-web-components/scripts.js',

];

await concat(files, './dist/sbi-web-components/sbiWebComponents.js');

// await fs.copyFile('./dist/iata-web-components/styles.css', './dist/iata-web-components/iataWebComponents.css')
await fs.copyFile('./dist/storybook1/styles.66e0a79c1f65f63a.css', './dist/sbi-web-components/sbiWebComponents.css')

})()